package com.ishom.footbalschedule.View.UI

import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import com.ishom.footbal_match_schedule.Presentation.bold
import com.ishom.footbal_match_schedule.R
import org.jetbrains.anko.*

class ScheduleUI : AnkoComponent<ViewGroup> {
    companion object {
        val txtSchedule = 1
        val txtLeague = 2
        val txtTeamLeft = 3
        val txtScoreLeft = 4
        val txtTeamRight = 5
        val txtScoreRight = 6
    }

    override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui) {
        verticalLayout {
            lparams(matchParent, wrapContent){
                topMargin = dip(6)
                bottomMargin = dip(6)
                leftMargin = dip(12)
                rightMargin = dip(12)
            }
            backgroundResource = R.drawable.white_shadow
            padding = dip(12)
            gravity = Gravity.CENTER_HORIZONTAL

            textView{
                id = txtLeague
                text = "League Name"
                textSize = 12f
                textColorResource = R.color.colorPrimary
            }.lparams(wrapContent, wrapContent){

            }

            textView{
                id = txtSchedule
                text = "Schedule"
                textSize = 16f
                textColorResource = R.color.black
            }.lparams(wrapContent, wrapContent){

            }

            linearLayout{
                lparams(matchParent, matchParent){
                    topMargin = dip(2)
                }
                gravity = Gravity.CENTER_VERTICAL

                linearLayout {
                    lparams(matchParent, wrapContent){
                        weight = 1f
                    }
                    gravity = Gravity.CENTER_VERTICAL

                    textView {
                        id = txtTeamLeft
                        text = "Left Team"
                        gravity = Gravity.CENTER
                        textSize = 16f
                        textColorResource = R.color.black
                    }.lparams(matchParent, wrapContent){
                        weight = 1f
                        rightMargin = dip(2)
                    }

                    textView {
                        id = txtScoreLeft
                        text = "0"
                        gravity = Gravity.CENTER
                        textSize = 28f
                        textColorResource = R.color.colorPrimaryDark
                    }.lparams(wrapContent, wrapContent)
                            .bold()
                }

                textView {
                    text = "-"
                    textSize = 14f
                    gravity = Gravity.CENTER
                }.lparams(wrapContent, matchParent){
                    margin = dip(2)
                }
                        .bold()

                linearLayout {
                    lparams(matchParent, wrapContent){
                        weight = 1f
                    }
                    gravity = Gravity.CENTER_VERTICAL

                    textView {
                        id = txtScoreRight
                        text = "0"
                        gravity = Gravity.CENTER
                        textSize = 28f
                        textColorResource = R.color.colorPrimaryDark
                    }.lparams(wrapContent, wrapContent)
                            .bold()

                    textView {
                        id = txtTeamRight
                        text = "Right Team"
                        gravity = Gravity.CENTER
                        textSize = 14f
                        textColorResource = R.color.black
                    }.lparams(matchParent, wrapContent){
                        weight = 1f
                        leftMargin = dip(2)
                    }
                }

            }

        }
    }
}


