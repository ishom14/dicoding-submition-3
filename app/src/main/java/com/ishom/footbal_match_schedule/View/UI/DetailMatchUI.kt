package com.ishom.footbalschedule.View.UI

import android.graphics.Color
import android.graphics.Typeface
import android.support.v4.content.ContextCompat
import android.support.v7.widget.Toolbar
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.ishom.footbal_match_schedule.Presentation.bold
import com.ishom.footbal_match_schedule.R
import com.ishom.footbal_match_schedule.View.Activity.DetailMatchActivity
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.toolbar

class DetailMatchUI : AnkoComponent<DetailMatchActivity> {
    companion object {
        lateinit var toolbar: Toolbar

        lateinit var txtSchedule: TextView
        lateinit var txtLeague: TextView
        lateinit var txtTeamLeft: TextView

        lateinit var txtScoreLeft: TextView
        lateinit var txtTeamRight: TextView
        lateinit var txtScoreRight: TextView

        lateinit var imgTeamRight : ImageView
        lateinit var imgTeamLeft : ImageView

        lateinit var imgSmallTeamRight1 : ImageView
        lateinit var imgSmallTeamLeft1 : ImageView

        lateinit var  imgSmallTeamRight2 : ImageView
        lateinit var imgSmallTeamLeft2 : ImageView

        lateinit var imgSmallTeamRight3 : ImageView
        lateinit var imgSmallTeamLeft3 : ImageView

        lateinit var txtShotLeft: TextView
        lateinit var txtShotRight: TextView

        lateinit var bgNotFound :View
        lateinit var bgMatchDetail :View

        lateinit var bgScoreDetail :View
        lateinit var bgScorePlayer :View
        lateinit var txtPlayerScoreLeft : TextView
        lateinit var txtPlayerScoreRight : TextView

        lateinit var bgYellowCard :View
        lateinit var txtPlayerYellowCardLeft : TextView
        lateinit var txtPlayerYellowCardRight  : TextView

        lateinit var bgRedCard :View
        lateinit var txtPlayerRedCardLeft : TextView
        lateinit var txtPlayerRedCardRight  : TextView

        lateinit var txtGoalKepperLeft : TextView
        lateinit var txtGoalKepperRight : TextView

        lateinit var txtDefenseLeft : TextView
        lateinit var txtDefenseRight : TextView

        lateinit var txtMidLeft : TextView
        lateinit var txtMidRight : TextView

        lateinit var txtFowardLeft : TextView
        lateinit var txtFowardRight : TextView

        lateinit var txtSubtitueLeft : TextView
        lateinit var txtSubtitueRight : TextView
    }

    override fun createView(ui: AnkoContext<DetailMatchActivity>): View = with(ui) {
        verticalLayout {
            lparams(matchParent, matchParent)

            //toolbar
            relativeLayout {
                lparams(matchParent, wrapContent)

                toolbar = toolbar {
                    backgroundColorResource = R.color.colorPrimary
                    lparams(matchParent, wrapContent)
                }

                textView {
                    text = "Match Detail"
                    textColorResource = R.color.white
                    textSize = 18f
                    gravity = Gravity.CENTER

                }.lparams(wrapContent, wrapContent){
                    centerInParent()
                }

            }

            scrollView {
                verticalLayout {
                    //league top
                    verticalLayout {

                        lparams(matchParent, wrapContent){
                            margin = dip(12)
                        }
                        backgroundResource = R.drawable.white_shadow
                        topPadding = dip(12)
                        bottomPadding = dip(14)
                        leftPadding = dip(16)
                        rightPadding = dip(16)
                        gravity = Gravity.CENTER_HORIZONTAL

                        txtLeague = textView{
                            text = "League Name"
                            textSize = 12f
                            textColorResource = R.color.colorPrimary
                        }.lparams(wrapContent, wrapContent){

                        }

                        txtSchedule = textView{
                            text = "Schedule"
                            textSize = 16f
                            textColorResource = R.color.black
                        }.lparams(wrapContent, wrapContent){

                        }

                        linearLayout{
                            lparams(matchParent, matchParent){
                                topMargin = dip(8)
                                bottomMargin = dip(8)
                            }
                            gravity = Gravity.CENTER_VERTICAL

                            linearLayout {
                                lparams(matchParent, wrapContent){
                                    weight = 1f
                                }
                                gravity = Gravity.CENTER_VERTICAL

                                verticalLayout {
                                    lparams(matchParent, wrapContent){
                                        weight = 1f
                                        leftMargin = dip(2)
                                    }
                                    gravity = Gravity.CENTER_HORIZONTAL

                                    imgTeamLeft = imageView{
                                    }.lparams(dip(56), dip(56)){
                                        bottomMargin = dip(2)
                                    }

                                    txtTeamLeft = textView {
                                        text = "Left Team"
                                        gravity = Gravity.CENTER
                                        textSize = 14f
                                        textColorResource = R.color.black
                                    }.lparams(matchParent, wrapContent){
                                        weight = 1f
                                        rightMargin = dip(2)
                                    }
                                }

                                txtScoreLeft = textView {
                                    text = ""
                                    gravity = Gravity.CENTER
                                    textSize = 32f
                                    typeface = Typeface.DEFAULT_BOLD
                                    textColorResource = R.color.colorPrimaryDark
                                }.lparams(wrapContent, wrapContent)
                            }

                            textView {
                                text = "-"
                                textSize = 14f
                                gravity = Gravity.CENTER
                            }.lparams(wrapContent, matchParent){
                                margin = dip(2)
                            }
                                    .bold()

                            linearLayout {
                                lparams(matchParent, wrapContent){
                                    weight = 1f
                                }
                                gravity = Gravity.CENTER_VERTICAL

                                txtScoreRight = textView {
                                    text = ""
                                    gravity = Gravity.CENTER
                                    textSize = 32f
                                    typeface = Typeface.DEFAULT_BOLD
                                    textColorResource = R.color.colorPrimaryDark
                                }.lparams(wrapContent, wrapContent)

                                verticalLayout {
                                    lparams(matchParent, wrapContent){
                                        weight = 1f
                                        leftMargin = dip(2)
                                    }
                                    gravity = Gravity.CENTER_HORIZONTAL

                                    imgTeamRight = imageView{
                                    }.lparams(dip(56), dip(56)){
                                        bottomMargin = dip(2)
                                    }

                                    txtTeamRight = textView {
                                        text = "Right Team"
                                        gravity = Gravity.CENTER
                                        textSize = 14f
                                        textColorResource = R.color.black
                                    }.lparams(matchParent, wrapContent){
                                        weight = 1f
                                        leftMargin = dip(2)
                                    }
                                }
                            }

                        }

                        //bgScoreDetail
                        bgScoreDetail = verticalLayout{
                            lparams(matchParent, wrapContent)

                            linearLayout{
                                lparams(matchParent, dip(1)){
                                    topMargin = dip(8)
                                    bottomMargin = dip(8)
                                }
                                backgroundColorResource = R.color.grey
                            }

                            //bgScore
                            bgScorePlayer = linearLayout{
                                lparams(matchParent, wrapContent){
                                    topMargin = dip(2)
                                }
                                gravity = Gravity.TOP

                                txtPlayerScoreLeft = textView{
                                    text = "PlayerName"
                                    gravity = Gravity.LEFT
                                }.lparams(matchParent, wrapContent){
                                    weight = 1f
                                }

                                imageView (R.drawable.soccerball){
                                }.lparams(dip(18), dip(18))

                                txtPlayerScoreRight = textView{
                                    text = "PlayerName"
                                    gravity = Gravity.RIGHT
                                }.lparams(matchParent, wrapContent){
                                    weight = 1f
                                }
                            }

                            //bgYellowCard
                            bgYellowCard = linearLayout{
                                lparams(matchParent, wrapContent){
                                    topMargin = dip(2)
                                }
                                gravity = Gravity.TOP

                                txtPlayerYellowCardLeft = textView{
                                    text = "PlayerName"
                                    gravity = Gravity.LEFT
                                }.lparams(matchParent, wrapContent){
                                    weight = 1f
                                }

                                imageView (R.drawable.ic_yellow_card_right){
                                }.lparams(dip(18), dip(18))

                                txtPlayerYellowCardRight = textView{
                                    text = "PlayerName"
                                    gravity = Gravity.RIGHT
                                }.lparams(matchParent, wrapContent){
                                    weight = 1f
                                }
                            }

                            //bgRedCard
                            bgRedCard = linearLayout{
                                lparams(matchParent, wrapContent){
                                    topMargin = dip(2)
                                }
                                gravity = Gravity.TOP

                                txtPlayerRedCardLeft = textView{
                                    text = "PlayerName"
                                    gravity = Gravity.LEFT
                                }.lparams(matchParent, wrapContent){
                                    weight = 1f
                                }

                                imageView (R.drawable.ic_red_card_right){
                                }.lparams(dip(18), dip(18))

                                txtPlayerRedCardRight = textView{
                                    text = "PlayerName"
                                    gravity = Gravity.RIGHT
                                }.lparams(matchParent, wrapContent){
                                    weight = 1f
                                }
                            }
                        }

                    }

                    //notfound
                    bgNotFound = verticalLayout{
                        visibility = View.GONE
                        lparams(matchParent, dip(72)){
                            leftMargin = dip(12)
                            rightMargin = dip(12)
                        }
                        backgroundResource = R.drawable.white_shadow
                        gravity = Gravity.CENTER

                        imageView(R.drawable.ic_block_black_24dp)
                                .lparams(dip(32), dip(32))

                        textView {
                            text = "Match Record Not Found"
                        }.lparams(wrapContent, wrapContent)
                    }

                    //match Detail
                    bgMatchDetail = verticalLayout {
                        //statistic
                        verticalLayout {

                            lparams(matchParent, wrapContent){
                                leftMargin = dip(12)
                                rightMargin = dip(12)
                            }
                            backgroundResource = R.drawable.white_shadow
                            topPadding = dip(12)
                            bottomPadding = dip(14)
                            leftPadding = dip(12)
                            rightPadding = dip(12)
                            gravity = Gravity.CENTER_HORIZONTAL

                            linearLayout{

                                linearLayout{
                                    lparams(matchParent, wrapContent){
                                        weight = 1f
                                    }
                                    gravity = Gravity.CENTER

                                    imgSmallTeamLeft1 = imageView{
                                    }.lparams(dip(24), dip(24))
                                }

                                textView{
                                    text = "TEAM STATS"
                                    textSize = 16f
                                    typeface = Typeface.DEFAULT_BOLD
                                    textColorResource = R.color.colorPrimaryDark
                                }.lparams(wrapContent, wrapContent)

                                linearLayout{
                                    lparams(matchParent, wrapContent){
                                        weight = 1f
                                    }
                                    gravity = Gravity.CENTER

                                    imgSmallTeamRight1 = imageView{
                                    }.lparams(dip(24), dip(24))
                                }
                            }

                            linearLayout{
                                lparams(matchParent, wrapContent){
                                    topMargin = dip(8)
                                }

                                linearLayout{
                                    lparams(matchParent, wrapContent){
                                        weight = 1f
                                    }
                                    gravity = Gravity.CENTER

                                    txtShotLeft = textView{
                                        text = "0"
                                    }.lparams(wrapContent, wrapContent)
                                }

                                textView{
                                    text = "Shot On Target"
                                    textSize = 14f
                                    textColorResource = R.color.black
                                }.lparams(wrapContent, wrapContent)

                                linearLayout{
                                    lparams(matchParent, wrapContent){
                                        weight = 1f
                                    }
                                    gravity = Gravity.CENTER

                                    txtShotRight = textView{
                                        text = "0"
                                    }.lparams(wrapContent, wrapContent)
                                }
                            }
                        }

                        //lineup
                        verticalLayout {

                            lparams(matchParent, wrapContent){
                                leftMargin = dip(12)
                                rightMargin = dip(12)
                                topMargin = dip(12)
                            }

                            backgroundResource = R.drawable.white_shadow
                            topPadding = dip(12)
                            bottomPadding = dip(14)
                            leftPadding = dip(16)
                            rightPadding = dip(16)
                            gravity = Gravity.CENTER_HORIZONTAL

                            linearLayout{

                                linearLayout{
                                    lparams(matchParent, wrapContent){
                                        weight = 1f
                                    }
                                    gravity = Gravity.CENTER

                                    imgSmallTeamLeft2 = imageView{
                                    }.lparams(dip(24), dip(24))
                                }

                                textView{
                                    text = "LINEUPS"
                                    textSize = 16f
                                    typeface = Typeface.DEFAULT_BOLD
                                    textColorResource = R.color.colorPrimaryDark
                                }.lparams(wrapContent, wrapContent)

                                linearLayout{
                                    lparams(matchParent, wrapContent){
                                        weight = 1f
                                    }
                                    gravity = Gravity.CENTER

                                    imgSmallTeamRight2 = imageView{
                                    }.lparams(dip(24), dip(24))
                                }
                            }

                            textView{
                                text = "GoalKepper"
                                textSize = 14f
                                textColorResource = R.color.colorPrimary
                            }.lparams(wrapContent, wrapContent){
                                topMargin = dip(8)
                            }

                            linearLayout{
                                lparams(matchParent, wrapContent){
                                    topMargin = dip(2)
                                }

                                linearLayout{
                                    lparams(matchParent, wrapContent){
                                        weight = 1f
                                    }
                                    gravity = Gravity.LEFT

                                    txtGoalKepperLeft = textView{
                                        text = "PlayerName"
                                    }.lparams(wrapContent, wrapContent)
                                }


                                linearLayout{
                                    lparams(matchParent, wrapContent){
                                        weight = 1f
                                    }
                                    gravity = Gravity.RIGHT

                                    txtGoalKepperRight =textView{
                                        text = "PlayerName"
                                        gravity = Gravity.RIGHT

                                    }.lparams(wrapContent, wrapContent)
                                }
                            }

                            textView{
                                text = "Defend"
                                textSize = 14f
                                textColorResource = R.color.colorPrimary
                            }.lparams(wrapContent, wrapContent){
                                topMargin = dip(8)
                            }

                            linearLayout{
                                lparams(matchParent, wrapContent){
                                    topMargin = dip(2)
                                }

                                linearLayout{
                                    lparams(matchParent, wrapContent){
                                        weight = 1f
                                    }
                                    gravity = Gravity.LEFT

                                    txtDefenseLeft = textView{
                                        text = "PlayerName"
                                    }.lparams(wrapContent, wrapContent)
                                }


                                linearLayout{
                                    lparams(matchParent, wrapContent){
                                        weight = 1f
                                    }
                                    gravity = Gravity.RIGHT

                                    txtDefenseRight = textView{
                                        text = "PlayerName"
                                        gravity = Gravity.RIGHT

                                    }.lparams(wrapContent, wrapContent)
                                }
                            }

                            textView{
                                text = "Midfield"
                                textSize = 14f
                                textColorResource = R.color.colorPrimary
                            }.lparams(wrapContent, wrapContent){
                                topMargin = dip(8)
                            }

                            linearLayout{
                                lparams(matchParent, wrapContent){
                                    topMargin = dip(2)
                                }

                                linearLayout{
                                    lparams(matchParent, wrapContent){
                                        weight = 1f
                                    }
                                    gravity = Gravity.LEFT

                                    txtMidLeft = textView{
                                        text = "PlayerName"
                                    }.lparams(wrapContent, wrapContent)
                                }


                                linearLayout{
                                    lparams(matchParent, wrapContent){
                                        weight = 1f
                                    }
                                    gravity = Gravity.RIGHT

                                    txtMidRight = textView{
                                        text = "PlayerName"
                                        gravity = Gravity.RIGHT
                                    }.lparams(wrapContent, wrapContent)
                                }
                            }

                            textView{
                                text = "Forward"
                                textSize = 14f
                                textColorResource = R.color.colorPrimary
                            }.lparams(wrapContent, wrapContent){
                                topMargin = dip(8)
                            }

                            linearLayout{
                                lparams(matchParent, wrapContent){
                                    topMargin = dip(2)
                                }

                                linearLayout{
                                    lparams(matchParent, wrapContent){
                                        weight = 1f
                                    }
                                    gravity = Gravity.LEFT

                                    txtFowardLeft =textView{
                                        text = "PlayerName"
                                        gravity = Gravity.RIGHT

                                    }.lparams(wrapContent, wrapContent)
                                }


                                linearLayout{
                                    lparams(matchParent, wrapContent){
                                        weight = 1f
                                    }
                                    gravity = Gravity.RIGHT

                                    txtFowardRight = textView{
                                        text = "PlayerName"
                                    }.lparams(wrapContent, wrapContent)
                                }
                            }
                        }

                        //subtitute
                        verticalLayout {

                            lparams(matchParent, wrapContent) {
                                margin = dip(12)
                            }

                            backgroundResource = R.drawable.white_shadow
                            topPadding = dip(12)
                            bottomPadding = dip(14)
                            leftPadding = dip(16)
                            rightPadding = dip(16)
                            gravity = Gravity.CENTER_HORIZONTAL

                            linearLayout {

                                linearLayout {
                                    lparams(matchParent, wrapContent) {
                                        weight = 1f
                                    }
                                    gravity = Gravity.CENTER

                                    imgSmallTeamLeft3 = imageView {
                                    }.lparams(dip(24), dip(24))
                                }

                                textView {
                                    text = "SUBTITUTE"
                                    textSize = 16f
                                    typeface = Typeface.DEFAULT_BOLD
                                    textColorResource = R.color.colorPrimaryDark
                                }.lparams(wrapContent, wrapContent)

                                linearLayout {
                                    lparams(matchParent, wrapContent) {
                                        weight = 1f
                                    }
                                    gravity = Gravity.CENTER

                                    imgSmallTeamRight3 = imageView {
                                    }.lparams(dip(24), dip(24))
                                }
                            }

                            linearLayout {
                                lparams(matchParent, wrapContent) {
                                    topMargin = dip(2)
                                }

                                linearLayout {
                                    lparams(matchParent, wrapContent) {
                                        weight = 1f
                                        rightMargin = dip(4)
                                    }
                                    gravity = Gravity.LEFT

                                    txtSubtitueLeft = textView {
                                        text = "PlayerName"
                                        gravity = Gravity.LEFT
                                    }.lparams(wrapContent, wrapContent)
                                }


                                linearLayout {
                                    lparams(matchParent, wrapContent) {
                                        weight = 1f
                                        leftMargin = dip(4)
                                    }
                                    gravity = Gravity.RIGHT

                                    txtSubtitueRight = textView {
                                        text = "PlayerName"
                                        gravity = Gravity.RIGHT
                                    }.lparams(wrapContent, wrapContent)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}


