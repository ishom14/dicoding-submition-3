package com.ishom.footbal_match_schedule.View.Activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import com.ishom.footbal_match_schedule.R
import com.ishom.footbal_match_schedule.R.id.*
import com.ishom.footbal_match_schedule.View.Fragment.FavoriteFragment
import com.ishom.footbal_match_schedule.View.Fragment.LastFragment
import com.ishom.footbal_match_schedule.View.Fragment.NextFragment
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        bottom_navigation_view.setOnNavigationItemSelectedListener {
            item -> when(item.itemId){
                last_match -> {
                    loadFragment(LastFragment(), savedInstanceState)
                }
                next_match -> {
                    loadFragment(NextFragment(), savedInstanceState)
                }
                favorite -> {
                    loadFragment(FavoriteFragment(), savedInstanceState)
                }
            }
            true
        }

        bottom_navigation_view.selectedItemId = last_match
    }

     fun loadFragment(fragment: Fragment, savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.frame, fragment)
                .commit()
        }
    }
}
