package com.ishom.footbal_match_schedule.View.Fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import com.ishom.footbal_match_schedule.Model.DatabaseHelper.Coloumn.MatchDetail
import com.ishom.footbal_match_schedule.Model.DatabaseHelper.database
import com.ishom.footbal_match_schedule.View.Adapter.ScheduleAnkoAdapter
import com.ishom.footbalschedule.Model.Object.Event
import org.jetbrains.anko.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.UI
import org.jetbrains.anko.support.v4.swipeRefreshLayout
import org.jetbrains.anko.support.v4.toast

class FavoriteFragment : Fragment() {

    lateinit var swipeRefreshLayout: SwipeRefreshLayout

    var listEvent : ArrayList<Event> = arrayListOf()
    lateinit var recycle: RecyclerView
    lateinit var recycleAdapter: ScheduleAnkoAdapter

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {

        val view2 = UI {
            linearLayout() {
                lparams (matchParent, wrapContent)
                orientation = LinearLayout.VERTICAL

                swipeRefreshLayout = swipeRefreshLayout {

                    relativeLayout{
                        lparams (matchParent, wrapContent)

                        recycle = recyclerView {
                            lparams (matchParent, wrapContent)
                            layoutManager = LinearLayoutManager(ctx) as RecyclerView.LayoutManager?
                        }
                    }
                }
            }
        }

        recycleAdapter = ScheduleAnkoAdapter(listEvent, context)
        recycle.adapter = recycleAdapter

        swipeRefreshLayout.setOnRefreshListener {
            showFavorite()
        }

        return view2.view
    }

    private fun showFavorite(){
        context?.database?.use {
            swipeRefreshLayout.isRefreshing = false
            val result = select(MatchDetail.TABLE_FAVORITE)
            listEvent.clear()
            val favorite = result.parseList(classParser<MatchDetail>())
            for (data:MatchDetail in favorite){
                listEvent.add(Event(data.idEvent,data.strLeague,data.dateEvent, data.idHomeTeam, data.strHomeTeam, data.intHomeScore, data.intHomeShots, data.strHomeGoalDetails, data.strHomeRedCards, data.strHomeYellowCards, data.strHomeLineupGoalkeeper, data.strHomeLineupDefense, data.strHomeLineupMidfield, data.strHomeLineupForward, data.strHomeLineupSubstitutes, data.idAwayTeam, data.strAwayTeam, data.intAwayScore, data.intAwayShots, data.strAwayGoalDetails, data.strAwayRedCards, data.strAwayYellowCards, data.strAwayLineupGoalkeeper, data.strAwayLineupDefense, data.strAwayLineupMidfield, data.strAwayLineupForward, data.strAwayLineupSubstitutes))
            }
            recycleAdapter.notifyDataSetChanged()
        }
    }

    override fun onResume() {
        super.onResume()
        showFavorite()
    }
}