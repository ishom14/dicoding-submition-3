package com.ishom.footbal_match_schedule.View.Fragment

import android.app.ProgressDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.LinearLayout
import android.widget.Spinner
import com.ishom.footbal_match_schedule.Presentation.Client.ClientInterface
import com.ishom.footbal_match_schedule.R
import com.ishom.footbal_match_schedule.View.Adapter.ScheduleAnkoAdapter
import com.ishom.footbal_match_schedule.View.Adapter.SpinnerAnkoAdapter
import com.ishom.footbalschedule.Model.Object.Event
import com.ishom.footbalschedule.Model.Object.League
import com.ishom.footbalschedule.Model.Object.SpinnerItem
import com.ishom.footbalschedule.Presentation.Client.Respone.FootballEventResponse
import com.ishom.footbalschedule.Presentation.Client.Respone.FootballLeagueResponse
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.UI
import org.jetbrains.anko.support.v4.swipeRefreshLayout
import org.jetbrains.anko.support.v4.toast
import retrofit2.Call
import retrofit2.Response
import javax.security.auth.callback.Callback

class NextFragment : Fragment() {
    var leagueID:String = ""

    lateinit var swipeRefreshLayout: SwipeRefreshLayout

    var listEvent : ArrayList<Event> = arrayListOf()
    lateinit var recycle: RecyclerView
    lateinit var recycleAdapter: ScheduleAnkoAdapter

    var listLiga : ArrayList<SpinnerItem> = arrayListOf()
    lateinit var spinner: Spinner
    lateinit var spinnerAdapter: SpinnerAnkoAdapter

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {

        val view2 = UI {
            linearLayout() {
                lparams (matchParent, wrapContent)
                orientation = LinearLayout.VERTICAL

                linearLayout{
                    lparams(matchParent, wrapContent){
                        margin = dip(12)
                        padding = dip(4)
                        backgroundResource = R.drawable.white_shadow

                        spinner = spinner {
                        }.lparams(matchParent, dip(48))
                    }
                }

                swipeRefreshLayout = swipeRefreshLayout {

                    relativeLayout{
                        lparams (matchParent, wrapContent)

                        recycle = recyclerView {
                            lparams (matchParent, wrapContent)
                            layoutManager = LinearLayoutManager(ctx)
                        }
                    }
                }
            }
        }

        spinnerAdapter = SpinnerAnkoAdapter(listLiga, context)
        spinner.adapter = spinnerAdapter
        spinnerAdapter.notifyDataSetChanged()

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
//                toast(listLiga[position].name).show()
                getEventData(listLiga[position].id)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }

        recycleAdapter = ScheduleAnkoAdapter(listEvent, context)
        recycle.adapter = recycleAdapter

        swipeRefreshLayout.setOnRefreshListener {
            getEventData(leagueID)
        }

        return view2.view
    }

    fun getLeagueData(){

        val dialog = ProgressDialog(activity)
        dialog.setMessage("Loading..")
        dialog.show()

        ClientInterface.create().getFootballLeague("Soccer")
                .enqueue(object: Callback, retrofit2.Callback<FootballLeagueResponse> {
                    override fun onResponse(call: Call<FootballLeagueResponse>, response: Response<FootballLeagueResponse>) {
                        if (response.isSuccessful){
                            dialog.hide()
                            var position: Int = 0
                            for (data : League in response.body()?.countrys!!){
                                listLiga.add(SpinnerItem(data.idLeague, data.strLeague))
                            }
                            spinnerAdapter.notifyDataSetChanged()

                        }else {
                            dialog.hide()
                            toast("Connection Error").show()
                        }
                    }

                    override fun onFailure(call: Call<FootballLeagueResponse>?, t: Throwable?) {
                        dialog.hide()
                        toast("Connection Error").show()
                    }
                })
    }


    fun getEventData(leagueID: String){

        swipeRefreshLayout.isRefreshing = false
        this.leagueID = leagueID

        val dialog = ProgressDialog(activity)
        dialog.setMessage("Loading..")
        dialog.show()

        ClientInterface.create().getNextEvent(leagueID)
                .enqueue(object: Callback, retrofit2.Callback<FootballEventResponse> {
                    override fun onResponse(call: Call<FootballEventResponse>, response: Response<FootballEventResponse>) {
                        if (response.isSuccessful){
                            dialog.hide()
                            listEvent.clear()
                            if (response.body()?.events != null){
                                for (data : Event in response.body()?.events!!){
                                    if (data.intAwayScore != null){
                                        listEvent.add(data)
                                    }else {
                                        listEvent.add(data)
                                    }
                                }
                            }else {
                                toast("Data not available").show()
                            }
                            recycleAdapter.updateData(listEvent)
                            recycleAdapter.notifyDataSetChanged()
                        }else {
                            dialog.hide()
                            toast("Connection Error").show()
                        }
                    }

                    override fun onFailure(call: Call<FootballEventResponse>?, t: Throwable?) {
                        dialog.hide()
                        toast("Connection Error").show()
                    }
                })
    }



    override fun onResume() {
        super.onResume()
        getLeagueData()
    }
}