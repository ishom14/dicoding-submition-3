package com.ishom.footbal_match_schedule.View.Activity

import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import com.ishom.footbal_match_schedule.Model.DatabaseHelper.Coloumn.MatchDetail
import com.ishom.footbal_match_schedule.Model.DatabaseHelper.database
import com.ishom.footbal_match_schedule.Presentation.Client.ClientInterface
import com.ishom.footbal_match_schedule.Presentation.dateModify
import com.ishom.footbal_match_schedule.Presentation.dayName
import com.ishom.footbal_match_schedule.R
import com.ishom.footbal_match_schedule.R.drawable.ic_star_empty
import com.ishom.footbal_match_schedule.R.drawable.ic_star_full
import com.ishom.footbalschedule.Model.Object.Event
import com.ishom.footbalschedule.Presentation.Client.Respone.FootballTeamResponse
import com.ishom.footbalschedule.View.UI.DetailMatchUI
import com.squareup.picasso.Picasso
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.setContentView
import org.jetbrains.anko.toast
import retrofit2.Call
import retrofit2.Response
import javax.security.auth.callback.Callback


class DetailMatchActivity : AppCompatActivity() {

    private var menuItem: Menu ? = null
    private var isFavorite: Boolean = false
    private lateinit var data: Event
    private lateinit var id: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DetailMatchUI().setContentView(this)

        val bundle = intent.extras
        data = bundle.getParcelable<Event>("data")
        id = data.idEvent!!

        favoriteState()

        setSupportActionBar(DetailMatchUI.toolbar)
        supportActionBar?.setTitle("")
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        DetailMatchUI.txtLeague.text = data.strLeague
        DetailMatchUI.txtSchedule.text = data.dateEvent?.dayName() + ", " + data.dateEvent?.dateModify()
        DetailMatchUI.txtTeamLeft.text = data.strHomeTeam
        DetailMatchUI.txtTeamRight.text = data.strAwayTeam

        if (data.intAwayScore == null){
            DetailMatchUI.bgMatchDetail.visibility = View.GONE
            DetailMatchUI.bgScoreDetail.visibility = View.GONE
            DetailMatchUI.bgNotFound.visibility = View.VISIBLE
        }else {
            DetailMatchUI.txtScoreLeft.text = data.intHomeScore
            DetailMatchUI.txtScoreRight.text = data.intAwayScore
        }

        //score player
        if (data.strAwayGoalDetails != null || data.strHomeGoalDetails != null ){
            if (data.strAwayGoalDetails != "" || data.strHomeGoalDetails != ""){
                DetailMatchUI.txtPlayerScoreLeft.text = data.strHomeGoalDetails?.replace(";", "\n")
                DetailMatchUI.txtPlayerScoreRight.text = data.strAwayGoalDetails?.replace(";", "\n")
            }else {
                DetailMatchUI.bgScorePlayer.visibility = View.GONE
            }
        }else {
            DetailMatchUI.bgScorePlayer.visibility = View.GONE
        }

        //yellow card
        if (data.strHomeYellowCards!= null || data.strAwayYellowCards != null ){
            if (data.strHomeYellowCards!= "" || data.strAwayYellowCards != "" ){
                DetailMatchUI.txtPlayerYellowCardLeft.text = data.strHomeYellowCards?.replace(";", "\n")
                DetailMatchUI.txtPlayerYellowCardRight.text = data.strAwayYellowCards?.replace(";", "\n")
            }else {
                DetailMatchUI.bgYellowCard.visibility = View.GONE
            }
        }else {
            DetailMatchUI.bgYellowCard.visibility = View.GONE
        }

        //red card
        if (data.strHomeRedCards!= null || data.strAwayRedCards != null ){
            if (data.strHomeRedCards!= "" || data.strAwayRedCards != "" ){
                DetailMatchUI.txtPlayerRedCardLeft.text = data.strHomeRedCards?.replace(";", "\n")
                DetailMatchUI.txtPlayerRedCardRight.text = data.strAwayRedCards?.replace(";", "\n")
            }else {
                DetailMatchUI.bgRedCard.visibility = View.GONE
            }
        }else {
            DetailMatchUI.bgRedCard.visibility = View.GONE
        }

        //shot on target
        if (data.intHomeShots != null || data.intAwayShots != null ){
                DetailMatchUI.txtShotLeft.text = data.intHomeShots
                DetailMatchUI.txtShotRight.text = data.intAwayShots
        }

        //lineups
        if (data.strHomeLineupGoalkeeper != null || data.strAwayLineupGoalkeeper != null){
            DetailMatchUI.txtGoalKepperLeft.text = data.strHomeLineupGoalkeeper?.replace(";", "\n")
            DetailMatchUI.txtDefenseLeft.text = data.strHomeLineupDefense?.replace(";", "\n")
            DetailMatchUI.txtMidLeft.text = data.strHomeLineupMidfield?.replace(";", "\n")
            DetailMatchUI.txtFowardLeft.text = data.strHomeLineupForward?.replace(";", "\n")
            DetailMatchUI.txtSubtitueLeft.text = data.strHomeLineupSubstitutes?.replace(";", "\n")

            DetailMatchUI.txtGoalKepperRight.text = data.strAwayLineupGoalkeeper?.replace(";", "\n")
            DetailMatchUI.txtDefenseRight.text = data.strAwayLineupDefense?.replace(";", "\n")
            DetailMatchUI.txtMidRight.text = data.strAwayLineupMidfield?.replace(";", "\n")
            DetailMatchUI.txtFowardRight.text = data.strAwayLineupForward?.replace(";", "\n")
            DetailMatchUI.txtSubtitueRight.text = data.strAwayLineupSubstitutes?.replace(";", "\n")
        }else {
            DetailMatchUI.txtGoalKepperLeft.text = "Not Found"
            DetailMatchUI.txtDefenseLeft.text = "Not Found"
            DetailMatchUI.txtMidLeft.text = "Not Found"
            DetailMatchUI.txtFowardLeft.text = "Not Found"
            DetailMatchUI.txtSubtitueLeft.text = "Not Found"

            DetailMatchUI.txtGoalKepperRight.text = "Not Found"
            DetailMatchUI.txtDefenseRight.text = "Not Found"
            DetailMatchUI.txtMidRight.text = "Not Found"
            DetailMatchUI.txtFowardRight.text = "Not Found"
            DetailMatchUI.txtSubtitueRight.text = "Not Found"
        }

        getTeamAway(data.idAwayTeam)
        getTeamHome(data.idHomeTeam)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.detail_menu, menu)
        menuItem = menu
        setFavorite()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            android.R.id.home -> {
                finish()
                true
            }
            R.id.add_to_favorite -> {
                //favorite
                if (isFavorite) removeFromFavorite() else addToFavorite(data)

                isFavorite = !isFavorite
                setFavorite()

                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }
    private fun removeFromFavorite(){
        try {
            database.use {
                delete(MatchDetail.TABLE_FAVORITE, "(MATCH_ID = {id})",
                    "id" to id)
            }
            toast("Removed to favorite").show()
        } catch (e: SQLiteConstraintException){
            toast(e.localizedMessage).show()
        }
    }

    private fun favoriteState(){
        database.use {
            val result = select(MatchDetail.TABLE_FAVORITE)
                .whereArgs("(MATCH_ID = {id})",
                    "id" to id)
            val favorite = result.parseList(classParser<MatchDetail>())
            if (!favorite.isEmpty()) isFavorite = true
        }
    }

    private fun setFavorite() {
        if (isFavorite)
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, ic_star_full)
        else
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, ic_star_empty)
    }

    private fun addToFavorite(data : Event){
        try {
            database.use {
                insert(MatchDetail.TABLE_FAVORITE,
                    MatchDetail.ID_EVENT to data.idEvent,
                    MatchDetail.MATCH_ID to data.idEvent,
                    MatchDetail.STR_LEAGUE to data.strLeague,
                    MatchDetail.DATE_EVENT to data.dateEvent,
                    MatchDetail.ID_HOME_TEAM to data.idHomeTeam,
                    MatchDetail.STR_HOME_TEAM to data.strHomeTeam,
                    MatchDetail.INT_HOME_SCORE to data.intHomeScore,
                    MatchDetail.INT_HOME_SHOTS to data.intHomeShots,
                    MatchDetail.STR_HOME_GOAL_DETAILS to data.strHomeGoalDetails,
                    MatchDetail.STR_HOME_RED_CARDS to data.strHomeRedCards,
                    MatchDetail.STR_HOME_YELLOW_CARDS to data.strHomeYellowCards,
                    MatchDetail.STR_HOME_LINEUP_GOALKEPPER to data.strHomeLineupGoalkeeper,
                    MatchDetail.STR_HOME_LINEUP_DEFENSE to data.strHomeLineupDefense,
                    MatchDetail.STR_HOME_LINEUP_MIDFELD to data.strHomeLineupMidfield,
                    MatchDetail.STR_HOME_LINEUP_FORWARD to data.strHomeLineupForward,
                    MatchDetail.STR_HOME_LINEUP_SUBTITUTES to data.strHomeLineupSubstitutes,
                    MatchDetail.ID_AWAY_TEAM to data.idAwayTeam,
                    MatchDetail.STR_AWAY_TEAM to data.strAwayTeam,
                    MatchDetail.INT_AWAY_SCORE to data.intAwayScore,
                    MatchDetail.INT_AWAY_SHOTS to data.intAwayShots,
                    MatchDetail.STR_AWAY_GOAL_DETAILS to data.strAwayGoalDetails,
                    MatchDetail.STR_AWAY_RED_CARDS to data.strAwayRedCards,
                    MatchDetail.STR_AWAY_YELLOW_CARDS to data.strAwayYellowCards,
                    MatchDetail.STR_AWAY_LINEUP_GOALKEPPER to data.strAwayLineupGoalkeeper,
                    MatchDetail.STR_AWAY_LINEUP_DEFENSE to data.strAwayLineupDefense,
                    MatchDetail.STR_AWAY_LINEUP_MIDFELD to data.strAwayLineupMidfield,
                    MatchDetail.STR_AWAY_LINEUP_FORWARD to data.strAwayLineupForward,
                    MatchDetail.STR_AWAY_LINEUP_SUBTITUTES to data.strAwayLineupSubstitutes)
            }
            toast("Add to favorite").show()
        } catch (e: SQLiteConstraintException){
            toast(e.localizedMessage).show()
        }
    }
    fun getTeamHome(teamHomeID: String?){
        ClientInterface.create().getTeamDetail(teamHomeID)
                .enqueue(object: Callback, retrofit2.Callback<FootballTeamResponse> {
                    override fun onResponse(call: Call<FootballTeamResponse>, response: Response<FootballTeamResponse>) {
                        if (response.isSuccessful){
                            DetailMatchUI.imgTeamLeft.setImageUrl(response.body()?.teams?.get(0)?.strTeamBadge!!)
                            DetailMatchUI.imgSmallTeamLeft1.setImageUrl(response.body()?.teams?.get(0)?.strTeamBadge!!)
                            DetailMatchUI.imgSmallTeamLeft2.setImageUrl(response.body()?.teams?.get(0)?.strTeamBadge!!)
                            DetailMatchUI.imgSmallTeamLeft3.setImageUrl(response.body()?.teams?.get(0)?.strTeamBadge!!)
                        }else {
                            toast("Connection Error").show()
                        }
                    }
                    override fun onFailure(call: Call<FootballTeamResponse>?, t: Throwable?) {
                        toast("Connection Error").show()
                    }
                })
    }

    fun getTeamAway(teamAwayID: String?){
        ClientInterface.create().getTeamDetail(teamAwayID)
                .enqueue(object: Callback, retrofit2.Callback<FootballTeamResponse> {
                    override fun onResponse(call: Call<FootballTeamResponse>, response: Response<FootballTeamResponse>) {
                        if (response.isSuccessful){
                            DetailMatchUI.imgTeamRight.setImageUrl(response.body()?.teams?.get(0)?.strTeamBadge!!)
                            DetailMatchUI.imgSmallTeamRight1.setImageUrl(response.body()?.teams?.get(0)?.strTeamBadge!!)
                            DetailMatchUI.imgSmallTeamRight2.setImageUrl(response.body()?.teams?.get(0)?.strTeamBadge!!)
                            DetailMatchUI.imgSmallTeamRight3.setImageUrl(response.body()?.teams?.get(0)?.strTeamBadge!!)
                        }else {
                            toast("Connection Error").show()
                        }
                    }
                    override fun onFailure(call: Call<FootballTeamResponse>?, t: Throwable?) {
                        toast("Connection Error").show()
                    }
                })
    }

    fun ImageView.setImageUrl(url : String){
        Picasso.get().load(url).into(this)
    }

}