package com.ishom.footbal_match_schedule.View.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.ishom.footbal_match_schedule.Presentation.dateModify
import com.ishom.footbal_match_schedule.Presentation.dayName
import com.ishom.footbal_match_schedule.View.Activity.DetailMatchActivity
import com.ishom.footbalschedule.Model.Object.Event
import com.ishom.footbalschedule.View.UI.ScheduleUI
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.startActivity

class ScheduleAnkoAdapter (var list: ArrayList<Event> = arrayListOf(), val context : Context) : RecyclerView.Adapter<ScheduleAnkoAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        return ViewHolder(ScheduleUI().createView(AnkoContext.Companion.create(context, parent) as AnkoContext<ViewGroup>))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val data = list[position]
        holder?.txtLeague?.text = data.strLeague
        holder?.txtSchedule?.text = data.dateEvent?.dayName() + ", " + data.dateEvent?.dateModify()
        holder?.txtTeamLeft?.text = data.strHomeTeam
        holder?.txtTeamRight?.text = data.strAwayTeam
        if (data.intAwayScore == null){
            holder?.txtScoreLeft?.text = ""
            holder?.txtScoreRight?.text = ""
        }else {
            holder?.txtScoreLeft?.text = data.intHomeScore
            holder?.txtScoreRight?.text = data.intAwayScore
        }

        holder?.view?.setOnClickListener(){
            context.startActivity<DetailMatchActivity>("data" to data);
        }
    }

    fun updateData(list: ArrayList<Event>){
        this.list.addAll(list)
    }

    inner class ViewHolder (item: View) : RecyclerView.ViewHolder(item) {
        var txtLeague = item.findViewById<TextView>(ScheduleUI.txtLeague)
        var txtSchedule = item.findViewById<TextView>(ScheduleUI.txtSchedule)
        var txtTeamLeft = item.findViewById<TextView>(ScheduleUI.txtTeamLeft)
        var txtTeamRight = item.findViewById<TextView>(ScheduleUI.txtTeamRight)
        var txtScoreLeft = item.findViewById<TextView>(ScheduleUI.txtScoreLeft)
        var txtScoreRight = item.findViewById<TextView>(ScheduleUI.txtScoreRight)
        var view = item
    }
}