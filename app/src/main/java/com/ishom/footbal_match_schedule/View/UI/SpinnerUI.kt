package com.ishom.footbalschedule.View.UI

import android.view.View
import android.view.ViewGroup
import com.ishom.footbal_match_schedule.R
import org.jetbrains.anko.*

class SpinnerUI : AnkoComponent<ViewGroup> {

    companion object {
        val txtName = 1
    }

    override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui) {
        verticalLayout {
            lparams(matchParent, wrapContent)
            padding = dip(12)

            textView {
                id = txtName
                textSize = 16f
                textColorResource = R.color.black
                text = "Spinner Item"
            }
        }
    }



}