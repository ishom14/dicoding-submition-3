package com.ishom.footbal_match_schedule.View.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.ishom.footbal_match_schedule.R
import com.ishom.footbalschedule.Model.Object.SpinnerItem
import org.jetbrains.anko.*

class SpinnerAnkoAdapter(var list: ArrayList<SpinnerItem> = arrayListOf(), var context: Context) : BaseAdapter() {
    var mInflator: LayoutInflater

    init {
        mInflator = LayoutInflater.from(context)
    }

    override fun getView(position: Int, convertView: View?, viewGroup : ViewGroup): View {
        return with(viewGroup.context){
            verticalLayout {
                lparams(matchParent, wrapContent)
                padding = dip(12)

                textView (list[position].name) {
                    textSize = 16f
                    textColorResource = R.color.black
                    text = "Spinner Item"
                }
            }
        }
    }

    override fun getItemId(p0: Int): Long {
        return 0
    }


    override fun getItem(p0: Int): SpinnerItem? {
        return list[p0];
    }


    override fun getCount(): Int {
        return list.size
    }
}