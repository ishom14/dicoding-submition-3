package com.ishom.footbal_match_schedule.Model.DatabaseHelper.Coloumn

class MatchDetail(var id: Long?,
                  var matchId: String?,
                  var idEvent: String?,
                  var strLeague: String?,
                  var dateEvent: String?,

                  var idHomeTeam: String?,
                  var strHomeTeam: String?,
                  var intHomeScore: String?,
                  var intHomeShots: String?,
                  var strHomeGoalDetails: String?,
                  var strHomeRedCards: String?,
                  var strHomeYellowCards: String?,
                  var strHomeLineupGoalkeeper: String?,
                  var strHomeLineupDefense: String?,
                  var strHomeLineupMidfield: String?,
                  var strHomeLineupForward: String?,
                  var strHomeLineupSubstitutes: String?,

                  var idAwayTeam: String?,
                  var strAwayTeam: String?,
                  var intAwayScore: String?,
                  var intAwayShots: String?,
                  var strAwayGoalDetails: String?,
                  var strAwayRedCards: String?,
                  var strAwayYellowCards: String?,
                  var strAwayLineupGoalkeeper: String?,
                  var strAwayLineupDefense: String?,
                  var strAwayLineupMidfield: String?,
                  var strAwayLineupForward: String?,
                  var strAwayLineupSubstitutes: String ?) {

    companion object {
        val TABLE_FAVORITE: String = "TABLE_MATCH_FAVORITE"
        val ID: String = "ID_MATCH_DB"
        val MATCH_ID: String = "MATCH_ID"

        val ID_EVENT: String = "ID_EVENT"
        val STR_LEAGUE: String = "STR_LEAGUE"
        val DATE_EVENT: String = "DATE_EVENT"

        val ID_HOME_TEAM: String  = "ID_HOME_TEAM"
        val STR_HOME_TEAM: String = "STR_HOME_TEAM"
        val INT_HOME_SCORE: String = "INT_HOME_SCORE"
        val INT_HOME_SHOTS: String = "INT_HOME_SHOTS"
        val STR_HOME_GOAL_DETAILS: String = "STR_HOME_GOAL_DETAILS"
        val STR_HOME_RED_CARDS: String = "STR_HOME_RED_CARDS"
        val STR_HOME_YELLOW_CARDS: String = "STR_HOME_YELLOW_CARDS"
        val STR_HOME_LINEUP_GOALKEPPER: String = "STR_HOME_LINEUP_GOALKEPPER"
        val STR_HOME_LINEUP_DEFENSE: String = "STR_HOME_LINEUP_DEFENSE"
        val STR_HOME_LINEUP_MIDFELD: String = "STR_HOME_LINEUP_MIDFELD"
        val STR_HOME_LINEUP_FORWARD: String = "STR_HOME_LINEUP_FORWARD"
        val STR_HOME_LINEUP_SUBTITUTES: String = "STR_HOME_LINEUP_SUBTITUTES"

        val ID_AWAY_TEAM: String  = "ID_AWAY_TEAM"
        val STR_AWAY_TEAM: String = "STR_AWAY_TEAM"
        val INT_AWAY_SCORE: String = "INT_AWAY_SCORE"
        val INT_AWAY_SHOTS: String = "INT_AWAY_SHOTS"
        val STR_AWAY_GOAL_DETAILS: String = "STR_AWAY_GOAL_DETAILS"
        val STR_AWAY_RED_CARDS: String = "STR_AWAY_RED_CARDS"
        val STR_AWAY_YELLOW_CARDS: String = "STR_AWAY_YELLOW_CARDS"
        val STR_AWAY_LINEUP_GOALKEPPER: String = "STR_AWAY_LINEUP_GOALKEPPER"
        val STR_AWAY_LINEUP_DEFENSE: String = "STR_AWAY_LINEUP_DEFENSE"
        val STR_AWAY_LINEUP_MIDFELD: String = "STR_AWAY_LINEUP_MIDFELD"
        val STR_AWAY_LINEUP_FORWARD: String = "STR_AWAY_LINEUP_FORWARD"
        val STR_AWAY_LINEUP_SUBTITUTES: String = "STR_AWAY_LINEUP_SUBTITUTES"
    }
}