package com.ishom.footbalschedule.Model.Object

class Schedule {
    var matchId: String = ""
    var league: String = ""
    var scheduleDate: String = ""
    var teamLeft : String = ""
    var scoreLeft : String = ""
    var teamRight : String = ""
    var scoreRight : String = ""

    constructor(machId: String, league: String, scheduleDate: String, teamLeft: String, teamRight: String){
        this.matchId = matchId
        this.league = league
        this.scheduleDate = scheduleDate
        this.teamLeft = teamLeft
        this.teamRight = teamRight
        this.scoreLeft = ""
        this.scoreRight = ""
    }

    constructor(machId: String, league: String, scheduleDate: String, teamLeft: String, teamRight: String, scoreLeft :String, scoreRight: String){
        this.matchId = matchId
        this.league = league
        this.scheduleDate = scheduleDate
        this.teamLeft = teamLeft
        this.teamRight = teamRight
        this.scoreLeft = scoreLeft
        this.scoreRight = scoreRight
    }
}