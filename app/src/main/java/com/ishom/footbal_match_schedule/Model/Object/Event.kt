package com.ishom.footbalschedule.Model.Object

import android.os.Parcel
import android.os.Parcelable

class Event(var idEvent: String? = null,
            var strLeague: String? = null,
            var dateEvent: String? = null,

            var idHomeTeam: String ? = null,
            var strHomeTeam: String ? = null,
            var intHomeScore: String ? = null,
            var intHomeShots: String ? = null,
            var strHomeGoalDetails: String ? = null,
            var strHomeRedCards: String ? = null,
            var strHomeYellowCards: String ? = null,
            var strHomeLineupGoalkeeper: String ? = null,
            var strHomeLineupDefense: String ? = null,
            var strHomeLineupMidfield: String ? = null,
            var strHomeLineupForward: String ? = null,
            var strHomeLineupSubstitutes: String ? = null,

            var idAwayTeam: String ? = null,
            var strAwayTeam: String ? = null,
            var intAwayScore: String ? = null,
            var intAwayShots: String ? = null,
            var strAwayGoalDetails: String ? = null,
            var strAwayRedCards: String ? = null,
            var strAwayYellowCards: String ? = null,
            var strAwayLineupGoalkeeper: String ? = null,
            var strAwayLineupDefense: String ? = null,
            var strAwayLineupMidfield: String ? = null,
            var strAwayLineupForward: String ? = null,
            var strAwayLineupSubstitutes: String ? = null) : Parcelable{

    constructor(parcel: Parcel) : this() {
        idEvent = parcel.readString()
        idHomeTeam = parcel.readString()
        idAwayTeam = parcel.readString()
        strLeague = parcel.readString()
        dateEvent = parcel.readString()
        strHomeTeam = parcel.readString()
        intHomeScore = parcel.readString()
        intHomeShots= parcel.readString()
        strHomeGoalDetails = parcel.readString()
        strHomeRedCards = parcel.readString()
        strHomeYellowCards = parcel.readString()
        strHomeLineupGoalkeeper = parcel.readString()
        strHomeLineupDefense = parcel.readString()
        strHomeLineupMidfield = parcel.readString()
        strHomeLineupForward = parcel.readString()
        strHomeLineupSubstitutes = parcel.readString()
        strAwayTeam = parcel.readString()
        intAwayScore = parcel.readString()
        intAwayShots = parcel.readString()
        strAwayGoalDetails = parcel.readString()
        strAwayRedCards = parcel.readString()
        strAwayYellowCards = parcel.readString()
        strAwayLineupGoalkeeper = parcel.readString()
        strAwayLineupDefense = parcel.readString()
        strAwayLineupMidfield = parcel.readString()
        strAwayLineupForward = parcel.readString()
        strAwayLineupSubstitutes = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(idEvent)
        parcel.writeString(idHomeTeam)
        parcel.writeString(idAwayTeam)
        parcel.writeString(strLeague)
        parcel.writeString(dateEvent)
        parcel.writeString(strHomeTeam)
        parcel.writeString(intHomeScore)
        parcel.writeString(intHomeShots)
        parcel.writeString(strHomeGoalDetails)
        parcel.writeString(strHomeRedCards)
        parcel.writeString(strHomeYellowCards)
        parcel.writeString(strHomeLineupGoalkeeper)
        parcel.writeString(strHomeLineupDefense)
        parcel.writeString(strHomeLineupMidfield)
        parcel.writeString(strHomeLineupForward)
        parcel.writeString(strHomeLineupSubstitutes)
        parcel.writeString(strAwayTeam)
        parcel.writeString(intAwayScore)
        parcel.writeString(intAwayShots)
        parcel.writeString(strAwayGoalDetails)
        parcel.writeString(strAwayRedCards)
        parcel.writeString(strAwayYellowCards)
        parcel.writeString(strAwayLineupGoalkeeper)
        parcel.writeString(strAwayLineupDefense)
        parcel.writeString(strAwayLineupMidfield)
        parcel.writeString(strAwayLineupForward)
        parcel.writeString(strAwayLineupSubstitutes)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Event> {
        override fun createFromParcel(parcel: Parcel): Event {
            return Event(parcel)
        }

        override fun newArray(size: Int): Array<Event?> {
            return arrayOfNulls(size)
        }
    }

}