package com.ishom.footbal_match_schedule.Model.DatabaseHelper

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.ishom.footbal_match_schedule.Model.DatabaseHelper.Coloumn.MatchDetail
import org.jetbrains.anko.db.*

class SQLiteHelper(ctx: Context) : ManagedSQLiteOpenHelper(ctx, "Dicoding.db", null, 1) {

    companion object {
        private var instance: SQLiteHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): SQLiteHelper {
            if (instance == null) {
                instance = SQLiteHelper(ctx.applicationContext)
            }
            return instance as SQLiteHelper
        }
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.createTable(MatchDetail.TABLE_FAVORITE, true,
            MatchDetail.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
            MatchDetail.MATCH_ID to TEXT,
            MatchDetail.ID_EVENT to TEXT,
            MatchDetail.STR_LEAGUE to TEXT,
            MatchDetail.DATE_EVENT to TEXT,
            MatchDetail.ID_HOME_TEAM to TEXT,
            MatchDetail.STR_HOME_TEAM to TEXT,
            MatchDetail.INT_HOME_SCORE to TEXT,
            MatchDetail.INT_HOME_SHOTS to TEXT,
            MatchDetail.STR_HOME_GOAL_DETAILS to TEXT,
            MatchDetail.STR_HOME_RED_CARDS to TEXT,
            MatchDetail.STR_HOME_YELLOW_CARDS to TEXT,
            MatchDetail.STR_HOME_LINEUP_GOALKEPPER to TEXT,
            MatchDetail.STR_HOME_LINEUP_DEFENSE to TEXT,
            MatchDetail.STR_HOME_LINEUP_MIDFELD to TEXT,
            MatchDetail.STR_HOME_LINEUP_FORWARD to TEXT,
            MatchDetail.STR_HOME_LINEUP_SUBTITUTES to TEXT,
            MatchDetail.ID_AWAY_TEAM to TEXT,
            MatchDetail.STR_AWAY_TEAM to TEXT,
            MatchDetail.INT_AWAY_SCORE to TEXT,
            MatchDetail.INT_AWAY_SHOTS to TEXT,
            MatchDetail.STR_AWAY_GOAL_DETAILS to TEXT,
            MatchDetail.STR_AWAY_RED_CARDS to TEXT,
            MatchDetail.STR_AWAY_YELLOW_CARDS to TEXT,
            MatchDetail.STR_AWAY_LINEUP_GOALKEPPER to TEXT,
            MatchDetail.STR_AWAY_LINEUP_DEFENSE to TEXT,
            MatchDetail.STR_AWAY_LINEUP_MIDFELD to TEXT,
            MatchDetail.STR_AWAY_LINEUP_FORWARD to TEXT,
            MatchDetail.STR_AWAY_LINEUP_SUBTITUTES to TEXT)    }

    override fun onUpgrade(db: SQLiteDatabase, p1: Int, p2: Int) {
        db.dropTable(MatchDetail.TABLE_FAVORITE, true)
    }
}
// Access property for Context
val Context.database: SQLiteHelper
    get() = SQLiteHelper.getInstance(applicationContext)
