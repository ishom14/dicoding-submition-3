package com.ishom.footbal_match_schedule.Presentation

import android.graphics.Typeface
import android.widget.TextView
import java.text.SimpleDateFormat
import java.util.*

fun TextView.bold() {
    setTypeface(null, Typeface.BOLD)
}

fun String.dateModify(): String{
    var strArrayDate = split("-".toRegex())
    var date = Date(strArrayDate[2].toInt(), strArrayDate[1].toInt(), strArrayDate[0].toInt())
    return strArrayDate[2] + " " + strArrayDate[1].toInt().dateName() + " " + strArrayDate[0];
}

fun String.enter(){
    replace(";", "\n")
}


fun String.dayName(): String {
    var strArrayDate = split("-".toRegex())
    var date = Date(strArrayDate[0].toInt(), strArrayDate[1].toInt(), strArrayDate[2].toInt())
    return SimpleDateFormat("EEEE", Locale.ENGLISH).format(date)
}

fun String.dateName(): String {
    var strArrayDate = split("-".toRegex())
    var date = Date(strArrayDate[2].toInt(), strArrayDate[1].toInt(), strArrayDate[0].toInt())
    return date.date.dateName()
}

fun Int.dateName(): String {
    when (this){
        1 -> return "January"
        2 -> return "February"
        3 -> return "March"
        4 -> return "April"
        5 -> return "May"
        6 -> return "July"
        7 -> return "June"
        8 -> return "Agust"
        9 -> return "September"
        10 -> return "October"
        11 -> return "November"
        12 -> return "December"
        else -> return ""
    }
}