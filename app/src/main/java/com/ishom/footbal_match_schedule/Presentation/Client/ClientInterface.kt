package com.ishom.footbal_match_schedule.Presentation.Client

import com.ishom.footbalschedule.Presentation.Client.Respone.FootballEventResponse
import com.ishom.footbalschedule.Presentation.Client.Respone.FootballLeagueResponse
import com.ishom.footbalschedule.Presentation.Client.Respone.FootballTeamResponse
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface ClientInterface {
    @GET("search_all_leagues.php")
    fun getFootballLeague(@Query("s") sport:String) : Call<FootballLeagueResponse>

    @GET("eventspastleague.php")
    fun getPastEvent(@Query("id") eventId: String) : Call<FootballEventResponse>

    @GET("lookupteam.php")
    fun getTeamDetail(@Query("id") eventId: String?) : Call<FootballTeamResponse>

    @GET("eventsnextleague.php")
    fun getNextEvent(@Query("id") eventId: String) : Call<FootballEventResponse>

    companion object {
        fun create(): ClientInterface {
            val retrofit = Retrofit.Builder()
                    .baseUrl("https://www.thesportsdb.com/api/v1/json/1/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

            return retrofit.create(ClientInterface::class.java);
        }
    }
}