package com.ishom.footbal_match_schedule.View.Activity


import android.annotation.SuppressLint
import android.support.test.espresso.Espresso
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.ishom.footbal_match_schedule.R
import junit.framework.TestCase.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.text.SimpleDateFormat
import java.util.*

@LargeTest
@RunWith(AndroidJUnit4::class)
class HomeActivityTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(HomeActivity::class.java)

    @Test
    fun homeActivityTest() {

//        onView(ViewMatchers.withId(R.id.last_match)).perform(ViewActions.click())
//
//        onView(ViewMatchers.withId(R.id.bottom_navigation_view))
//            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
//        onView(ViewMatchers.withId(R.id.next_match)).perform(ViewActions.click())
//
//        onView(ViewMatchers.withId(R.id.bottom_navigation_view))
//            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
//        onView(ViewMatchers.withId(R.id.favorite)).perform(ViewActions.click())
    }

    @SuppressLint("SimpleDateFormat")
    fun toSimpleString(date: Date?): String? = with(date ?: Date()) {
        SimpleDateFormat("EEE, dd MM yyy").format(this)
    }
}
